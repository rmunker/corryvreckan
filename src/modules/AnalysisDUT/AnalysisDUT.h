/**
 * @file
 * @brief Definition of module AnalysisDUT
 *
 * @copyright Copyright (c) 2017-2020 CERN and the Corryvreckan authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#ifndef CORRYVRECKAN_DUT_ANALYSIS_H
#define CORRYVRECKAN_DUT_ANALYSIS_H

#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TProfile2D.h>
#include <iostream>
#include "core/module/Module.hpp"

namespace corryvreckan {
    /** @ingroup Modules
     */
    class AnalysisDUT : public Module {

    public:
        // Constructors and destructors
        AnalysisDUT(Configuration& config, std::shared_ptr<Detector> detector);
        ~AnalysisDUT() {}

        // Functions
        void initialize() override;
        StatusCode run(const std::shared_ptr<Clipboard>& clipboard) override;
        void finalize(const std::shared_ptr<ReadonlyClipboard>& clipboard) override;
        double correctTimewalk(std::vector<double> correctionParameters, double raw, double tdistance);

    private:
        std::shared_ptr<Detector> m_detector;

        // Histograms
        TH2F *hClusterMapAssoc, *hHitMapAssoc, *hHitMapROI;
        TProfile2D *hClusterSizeMapAssoc, *hClusterChargeMapAssoc, *hTimingResidualMapAssoc, *hPixelRawValueMapAssoc;

        TH1F* hPixelRawValueAssoc;
        TH2F* hPixelRawSaturationMapAssoc;
	TH2F* hTimingResidualMapDebug;

        TH1F* associatedTracksVersusTime;
        TH1F *residualsX, *residualsY, *residualsPos;
        TH2F* residualsPosVsresidualsTime;

        TH1F *residualsX1pix, *residualsY1pix;
        TH1F *residualsX2pix, *residualsY2pix;
        TH1F *residualsX3pix, *residualsY3pix;
        TH1F *residualsX4pix, *residualsY4pix;

        TH1F* clusterChargeAssoc;
        TH1F* clusterSizeAssoc;
        TH1F* clusterSizeXAssoc;
        TH1F* clusterSizeYAssoc;
        TH1F* clusterSizeAssocNorm;
        TH1F* clusterWidthRowAssoc;
        TH1F* clusterWidthColAssoc;

        TH1F* hCutHisto;

        TProfile2D *rmsxvsxmym, *rmsyvsxmym, *rmsxyvsxmym;
        TProfile2D *qvsxmym, *qMoyalvsxmym, *pxqvsxmym;
        TProfile2D *npxvsxmym, *inpixel_csX, *inpixel_csY, *inpixel_timeRes;
        TH2F *npx1vsxmym, *npx2vsxmym, *npx3vsxmym, *npx4vsxmym;

        TH1F* hTrackCorrelationX;
        TH1F* hTrackCorrelationY;
        TH1F* hTrackCorrelationPos;
        TH2F* hTrackCorrelationPosVsCorrelationTime;
        TH1F* hTrackCorrelationTime;
        TH1F* hTrackZPosDUT;
        TH1F* residualsTime;
        TH1F* residualsTime_1pixel;
        TH1F* residualsTime_2pixel;
        TH1F* residualsTime_3pixel;
        TH1F* residualsTime_4pixel;
        TH1F* residualsTime_fastest;
        TH1F* residualsTime_1pixel_fastest;
        TH1F* residualsTime_2pixel_fastest;
        TH1F* residualsTime_3pixel_fastest;
        TH1F* residualsTime_4pixel_fastest;

        TH2F* residualsTimeVsTot;
        TH2F* residualsTimeVsTime;
        TH2F* residualsTimeVsSignal;
        TH2F* residualsTimeVsRaw_seed;
        TH2F* residualsTimeVsRaw_seed_fastest;
        TH2F* residualsTimeVsRaw;
        TH2F* residualsTimeVsRaw_1pixel;
        TH2F* residualsTimeVsRaw_2pixel;
        TH2F* residualsTimeVsRaw_3pixel;
        TH2F* residualsTimeVsRaw_4pixel;
        TH2F* residualsTimeVsRaw_1pixel_fastest;
        TH2F* residualsTimeVsRaw_2pixel_fastest;
        TH2F* residualsTimeVsRaw_3pixel_fastest;
        TH2F* residualsTimeVsRaw_4pixel_fastest;

        TH2F* hAssociatedTracksGlobalPosition;
        TH2F* hAssociatedTracksLocalPosition;
        TH2F* hUnassociatedTracksGlobalPosition;
        // Member variables
        double m_timeCutFrameEdge;
        double chi2ndofCut;
        bool useClosestCluster;
        int num_tracks;
       
	std::vector< std::vector<double> > timewalkParameter;
	std::vector<double> timewalkParameter1;
	std::vector<double> timewalkParameter2;
	std::vector<double> timewalkParameter3;
    };
} // namespace corryvreckan

#endif // CORRYVRECKAN_DUT_ANALYSIS_H
