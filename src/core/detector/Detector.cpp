/** @file
 *  @brief Implementation of the detector model
 *  @copyright Copyright (c) 2017-2020 CERN and the Corryvreckan authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include <fstream>
#include <map>
#include <string>

#include "Math/RotationX.h"
#include "Math/RotationY.h"
#include "Math/RotationZ.h"
#include "Math/RotationZYX.h"

#include "Detector.hpp"
#include "core/utils/log.h"
#include "exceptions.h"

using namespace ROOT::Math;
using namespace corryvreckan;

Detector::Detector(const Configuration& config) : m_role(DetectorRole::NONE) {

    // Role of this detector:
    auto roles = config.getArray<std::string>("role", std::vector<std::string>{"none"});
    for(auto& role : roles) {
        std::transform(role.begin(), role.end(), role.begin(), ::tolower);
        if(role == "none") {
            m_role |= DetectorRole::NONE;
        } else if(role == "reference" || role == "ref") {
            m_role |= DetectorRole::REFERENCE;
        } else if(role == "dut") {
            m_role |= DetectorRole::DUT;
        } else if(role == "auxiliary" || role == "aux") {
            m_role |= DetectorRole::AUXILIARY;
        } else {
            throw InvalidValueError(config, "role", "Detector role does not exist.");
        }
    }

    // Auxiliary devices cannot hold other roles:
    if(static_cast<bool>(m_role & DetectorRole::AUXILIARY) && m_role != DetectorRole::AUXILIARY) {
        throw InvalidValueError(config, "role", "Auxiliary devices cannot hold any other detector role");
    }

    m_detectorName = config.getName();

    // Material budget of detector, including support material
    if(!config.has("material_budget")) {
        m_materialBudget = 0.0;
        LOG(WARNING) << "No material budget given for " << m_detectorName << ", assuming " << m_materialBudget;
    } else if(config.get<double>("material_budget") < 0) {
        throw InvalidValueError(config, "material_budget", "Material budget has to be positive");
    } else {
        m_materialBudget = config.get<double>("material_budget");
    }

    // Detector type
    m_detectorType = config.get<std::string>("type");
    std::transform(m_detectorType.begin(), m_detectorType.end(), m_detectorType.begin(), ::tolower);

    // Time offset 
    m_timeOffset = config.get<double>("time_offset", 0.0);
    if(m_timeOffset > 0.) {
        LOG(TRACE) << "Time offset: " << m_timeOffset;
    }

    // Timewalk correction parameter
    m_timewalkParameter0 =  config.get<double>("timewalk_par0", 0.0);
    m_timewalkParameter1 =  config.get<double>("timewalk_par1", 0.0);
    m_timewalkParameter2 =  config.get<double>("timewalk_par2", 0.0);
    m_timewalkParameter3 =  config.get<double>("timewalk_par3", 0.0);
    m_timewalkParameter4 =  config.get<double>("timewalk_par4", 0.0);
    m_timewalkParameter5 =  config.get<double>("timewalk_par5", 0.0);
    m_timewalkParameter6 =  config.get<double>("timewalk_par6", 0.0);
    // Timewalk correction file
    if(config.has("timewalk_file")) {
        m_timewalkfile_name = config.get<std::string>("timewalk_file");
        std::string timewalk_file = config.getPath("timewalk_file");
        LOG(INFO) << "Adding timewalk correction file to detector \"" << config.getName() << "\", reading from " << timewalk_file;
        set_timewalk_file(timewalk_file);
	process_timewalk_file();
   }

    // Time resolution - default to negative number, i.e. unknown. This will trigger an exception
    // when calling getTimeResolution
    m_timeResolution = config.get<double>("time_resolution", -1.0);
    if(m_timeResolution > 0) {
        LOG(TRACE) << "  Time resolution: " << Units::display(m_timeResolution, {"ms", "us"});
    }

    // Calibration file
    if(config.has("calibration_file")) {
      m_calibrationfile = config.getPath("calibration_file", true);
      LOG(DEBUG) << "Found calibration file for detector " << getName() << " at \"" <<m_calibrationfile << "\"";
    }


}

std::shared_ptr<Detector> corryvreckan::Detector::factory(const Configuration& config) {
    // default coordinate is cartesian coordinate
    std::string coordinates = config.get<std::string>("coordinates", "cartesian");
    std::transform(coordinates.begin(), coordinates.end(), coordinates.begin(), ::tolower);
    if(coordinates == "cartesian") {
        return std::make_shared<PixelDetector>(config);
    } else {
        throw InvalidValueError(config, "coordinates", "Coordinates can only set to be cartesian now");
    }
}

double Detector::getTimeResolution() const {
    if(m_timeResolution > 0) {
        return m_timeResolution;
    } else {
        throw InvalidSettingError(this, "time_resolution", "Time resolution not set but requested");
    }
}

std::string Detector::getName() const {
    return m_detectorName;
}

std::string Detector::getType() const {
    return m_detectorType;
}

bool Detector::isReference() const {
    return static_cast<bool>(m_role & DetectorRole::REFERENCE);
}

bool Detector::isDUT() const {
    return static_cast<bool>(m_role & DetectorRole::DUT);
}

bool Detector::isAuxiliary() const {
    return static_cast<bool>(m_role & DetectorRole::AUXILIARY);
}

// Functions to set and check channel masking
void Detector::set_mask_file(std::string file) {
    m_maskfile = file;
}

// Function to set timewalk correction file
void Detector::set_timewalk_file(std::string file) {
    m_timewalkfile = file;
}

void Detector::process_timewalk_file() {
    // Open the file with the timewalk correction parameters
    std::ifstream inputTimewalkFile(m_timewalkfile, std::ios::in);
    if(!inputTimewalkFile.is_open()) {
        LOG(ERROR) << "Could not open file for timewalk correction " << m_timewalkfile;
    } else {
        LOG(INFO) << "Open file for timewalk correction " << m_timewalkfile;
        double param;
        std::string id;
        // loop over all lines and get timewalk correction parameters
        while(inputTimewalkFile >> id) {
            LOG(INFO) << "Read from timewalk file" << id;
            if(id == "c1") {
                inputTimewalkFile >> param;
		m_timewalk_param1.push_back(param);
                LOG(INFO) << "Set parameter for 1-pixel cluster " << param;
            } else {
	      if(id == "c2") {
                inputTimewalkFile >> param;
		m_timewalk_param2.push_back(param);
                LOG(INFO) << "Set parameter for 2-pixel cluster " << param;
	      }
	      else{
		if(id == "c3") {
		  inputTimewalkFile >> param;
		  m_timewalk_param3.push_back(param);
		  LOG(INFO) << "Set parameter for 3-pixel cluster " << param;
		}
		else{
		  LOG(ERROR) << "Could not parse timewalk paramter entry (id \"" << id << "\"). Please use the syntax: \"c\"    parameter, staring with the lower exponents of the polynomial at the top of the file.";
		}
	      }
            }
        }
        LOG(INFO) << m_timewalk_param1.size() << " parameter for timewalk correction for 1-pixel clusters";
        LOG(INFO) << m_timewalk_param2.size() << " parameter for timewalk correction for 2-pixel clusters";
        LOG(INFO) << m_timewalk_param3.size() << " parameter for timewalk correction for 3-pixel clusters";

	m_timewalk_param.push_back(m_timewalk_param1);
	m_timewalk_param.push_back(m_timewalk_param2);
	m_timewalk_param.push_back(m_timewalk_param3);
    }
}


void Detector::update() {
  this->initialise();
}


Configuration Detector::getConfiguration() const {

    Configuration config(getName());
    config.set("type", m_detectorType);

    // Store the role of the detector
    std::vector<std::string> roles;
    if(this->isDUT()) {
        roles.push_back("dut");
    }
    if(this->isReference()) {
        roles.push_back("reference");
    }
    if(this->isAuxiliary()) {
        roles.push_back("auxiliary");
    }

    if(!roles.empty()) {
        config.setArray("role", roles);
    }

    if(m_timeOffset != 0.) {
        config.set("time_offset", m_timeOffset, {"ns", "us", "ms", "s"});
    }

    config.set("time_resolution", m_timeResolution, {"ns", "us", "ms", "s"});

    // different for PixelDetector and StripDetector
    this->configure_pos_and_orientation(config);

    // material budget
    if(m_materialBudget > std::numeric_limits<double>::epsilon()) {
        config.set("material_budget", m_materialBudget);
    }

    // Set timewalk parameter
    if(!m_timewalkfile_name.empty()) {
	  config.set("timewalk_file", m_timewalkfile_name);
	  config.set("timewalk_par0", m_timewalkParameter0, {"ns", "us", "ms", "s"});
	  config.set("timewalk_par1", m_timewalkParameter1, {"ns", "us", "ms", "s"});
	  config.set("timewalk_par2", m_timewalkParameter2, {"ns", "us", "ms", "s"});
	  config.set("timewalk_par3", m_timewalkParameter3, {"ns", "us", "ms", "s"});
	  config.set("timewalk_par4", m_timewalkParameter4, {"ns", "us", "ms", "s"});
	  config.set("timewalk_par5", m_timewalkParameter5, {"ns", "us", "ms", "s"});
	  config.set("timewalk_par6", m_timewalkParameter6, {"ns", "us", "ms", "s"});
     }

    // only if detector is not auxiliary:
    if(!this->isAuxiliary()) {
        this->configure_detector(config);
    }

    return config;
}
